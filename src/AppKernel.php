<?php

/**
 * console-skeleton
 * @author  michel <michel@mlequer.com> - https://mlequer.com
 * @license see LICENSE included in package
 * @version 1.0.0
 *
 **/

namespace MLequer;

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use MLequer\DependencyInjection\CompilerPass\CollectCommandsToApplicationCompilerPass;

final class AppKernel extends Kernel
{
    /**
     * In more complex app, add bundles here
     * @return array<mixed>
     */
    public function registerBundles(): array
    {
        return [];
    }

    /**
     * Load all services
     */
    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(__DIR__ . '/../config/services.yml');
    }



    protected function build(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->addCompilerPass(new CollectCommandsToApplicationCompilerPass());
    }
}
