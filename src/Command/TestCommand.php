<?php

/**
 * console-skeleton
 * @author  michel <michel@mlequer.com> - https://mlequer.com
 * @license see LICENSE included in package
 * @version 1.0.0
 *
 **/

namespace MLequer\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'test';

    protected function configure(): void
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Just a test command')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows to test the setup.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Test Command autoloaded ok',
            '============',
            '',
        ]);

        return 0;
    }
}
